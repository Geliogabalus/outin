import { Component, OnInit } from '@angular/core';
import { Venue } from '../venues.models';

@Component({
  selector: 'app-venues',
  templateUrl: './venues.component.html',
  styleUrls: ['./venues.component.scss']
})
export class VenuesComponent implements OnInit {
  mode: 'list' | 'map' = 'list';
  selectedVenue: Venue | null = null;
  constructor() { }

  ngOnInit(): void {
  }

  onVenueSelected(venue: Venue) {
    this.selectedVenue = venue;
  }

}
