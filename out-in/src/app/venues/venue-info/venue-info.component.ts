import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Venue } from '../venues.models';

@Component({
  selector: 'app-venue-info',
  templateUrl: './venue-info.component.html',
  styleUrls: ['./venue-info.component.scss']
})
export class VenueInfoComponent implements OnInit, OnChanges {
  @Input() venue: Venue | null = null;
  images: string[] = [];
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['venue']) {
      if (this.venue && this.venue.media) {
        this.images = this.venue.media.map(m => m.url)
      } else {
        this.images = []
      }
      console.log(this.images)
    }
  }
}
