import { Injectable } from '@angular/core';
import venuesData from '../../assets/data/establishment-data.json';
import eventsData from '../../assets/data/events-data.json';
import { Venue, Event } from './venues.models';

export interface Point {
  latitude: number;
  longitude: number;
}

@Injectable({
  providedIn: 'root'
})
export class VenuesService {
  venues: Venue[] = <Venue[]>venuesData.filter((v, i, a) => a.findIndex(t => (t.trcid === v.trcid)) === i);
  events: Venue[] = <Venue[]>eventsData.filter((v, i, a) => a.findIndex(t => (t.trcid === v.trcid)) === i);

  constructor() { 
    console.log(this.events)
  }

  getVenues(): Venue[] {
    return this.venues;
  }

  getEvents(): Venue[] {
    return this.events;
  }

  getNearbyEvents(venue: Venue): Event[] {
    const venuePoint: Point = {
      latitude: Number(venue?.location.latitude.replace(',','.')),
      longitude: Number(venue?.location.longitude.replace(',','.'))
    }

    const venueEvents: Event[] = [];
    this.events.forEach(ev => {
      const distance = this.coordinatesDistance(venuePoint, {
        latitude: Number(ev.location.latitude.replace(',','.')),
        longitude: Number(ev.location.longitude.replace(',','.'))
      });
      if (distance < 1) {
        (<Event>ev).distance = distance * 1000;
        venueEvents.push(<Event>ev);
      }
    })
    return venueEvents;
  }

  coordinatesDistance(point1: Point, point2: Point) {
    const degreesToRadians = (deg: number) => (deg * Math.PI) / 180.0;
    const earthRadius = 6371;
    const dLatitude = degreesToRadians(point2.latitude - point1.latitude);
    const dLongitude = degreesToRadians(point2.longitude - point1.longitude);
    const latitude1 = degreesToRadians(point1.latitude);
    const latitude2 = degreesToRadians(point2.latitude);

    var a = Math.sin(dLatitude/2) * Math.sin(dLatitude/2) +
    Math.sin(dLongitude/2) * Math.sin(dLongitude/2) * Math.cos(latitude1) * Math.cos(latitude2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    return earthRadius * c;
  }
}
