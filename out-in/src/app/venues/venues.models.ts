export interface Venue {
  dates: {
    startdate: string,
    enddate: string
  },
  details: { [key: string]: {
    language: string,
    calendarsummary: string,
    longdescription: string,
    shortdescription: string,
    title: string
  }},
  eigenschappen: { [key: number]: {
    Category: string,
    CategoryArea: string,
    Catid: string,
    Value: string
  }},
  lastupdated: string,
  location: {
    name: string,
    city: string,
    latitude: string,
    longitude: string,
    adress: string,
    zipcode: string
  },
  media: {
    main: string,
    url: string
  }[],
  title: string,
  trcid: string,
  types: {
    type: string,
    catid: string
  }[],
  urls: string[]
}

export interface Event extends Venue {
  distance: number;
}