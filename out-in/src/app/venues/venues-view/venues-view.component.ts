import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { VenuesService } from '../venues.service';
import DevExpress from 'devextreme';
import { Venue } from '../venues.models';
import { DateTime } from 'luxon';

@Component({
  selector: 'app-venues-view',
  templateUrl: './venues-view.component.html',
  styleUrls: ['./venues-view.component.scss']
})
export class VenuesViewComponent implements OnInit {
  venues: Venue[] = [];
  venuesColumns: DevExpress.ui.dxDataGridColumn[] = [];
  @Input() selectedVenue: Venue | null = null;
  @Output() selectionChange = new EventEmitter<Venue>();

  constructor(
    private venuesService: VenuesService
  ) { 
    this.venuesColumns = [
      {
        dataField: 'title',
        caption: 'Name',
        allowHeaderFiltering: false,
        width: "35%"
      },
      {
        dataField: 'location.city',
        caption: 'City',
        width: "15%"
      },
      {
        dataField: 'dates.startdate',
        caption: 'Start year',
        dataType: 'date',
        calculateCellValue: (venue: Venue) => {
          const value = venue?.dates.startdate;
          return value ? DateTime.fromFormat(value, "dd-MM-yyyy").toJSDate() : null;
        },
        calculateDisplayValue: (venue: Venue) => {
          const value = venue?.dates.startdate;
          return value ? DateTime.fromFormat(value, "dd-MM-yyyy").toLocaleString() : null;
        },
        width: "15%"
      },
      {
        dataField: 'location.adress',
        caption: 'Address',
        allowHeaderFiltering: false,
        width: "20%"
      },
      {
        dataField: 'location.zipcode',
        caption: 'Postcode',
        allowHeaderFiltering: false,
        width: "15%"
      }
    ]

    this.venues = this.venuesService.getVenues();
    console.log(this.venues)
  }

  onSelectionChange(e: any) {
    this.selectedVenue = e.selectedRowsData[0]
    this.selectionChange.emit(e.selectedRowsData[0])
  }

  ngOnInit(): void {
  }

}
