import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VenuesRoutingModule } from './venues-routing.module';
import { VenuesComponent } from './venues/venues.component';
import { VenuesViewComponent } from './venues-view/venues-view.component';
import { EventsViewComponent } from './events-view/events-view.component';
import { MapViewComponent } from './map-view/map-view.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { DxDataGridModule, DxGalleryModule, DxMapModule } from 'devextreme-angular';
import { VenueInfoComponent } from './venue-info/venue-info.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

@NgModule({
  declarations: [
    VenuesComponent,
    VenuesViewComponent,
    EventsViewComponent,
    MapViewComponent,
    VenueInfoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    VenuesRoutingModule,
    NgbModule,
    DxDataGridModule,
    DxGalleryModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoiZ2VsaW9nYWJhbHVzIiwiYSI6ImNrdTJyejRwYTJyeG4yb21ydDJiZDdwMWMifQ.260ENHGYZueViAwOwFC6bg'
    })
  ]
})
export class VenuesModule { }