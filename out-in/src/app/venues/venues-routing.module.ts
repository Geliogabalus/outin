import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VenuesComponent } from './venues/venues.component';

const routes: Routes = [
  {
    path: 'venues',
    component: VenuesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class VenuesRoutingModule { }
