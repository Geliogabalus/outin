import { Component, Input, OnInit } from '@angular/core';
import DevExpress from 'devextreme';
import { DateTime } from 'luxon';
import { Venue, Event } from '../venues.models';
import { VenuesService } from '../venues.service';

@Component({
  selector: 'app-events-view',
  templateUrl: './events-view.component.html',
  styleUrls: ['./events-view.component.scss']
})
export class EventsViewComponent implements OnInit {
  @Input() venue!: Venue;
  eventsColumns: DevExpress.ui.dxDataGridColumn[] = [];
  events: Venue[] = [];
  venueEvents: Event[] = [];

  constructor(
    private venuesService: VenuesService) { 
    this.events = this.venuesService.getEvents();

    this.eventsColumns = [
      {
        dataField: 'title',
        caption: 'Name',
        allowHeaderFiltering: false
      },
      /* {
        dataField: 'dates.startdate',
        caption: 'Start year',
        dataType: 'date',
        calculateCellValue: (venue: Venue) => {
          const value = venue?.dates.startdate;
          return value ? DateTime.fromFormat(value, "dd-MM-yyyy").toJSDate() : null;
        },
        calculateDisplayValue: (venue: Venue) => {
          const value = venue?.dates.startdate;
          return value ? DateTime.fromFormat(value, "dd-MM-yyyy").toLocaleString() : null;
        },
      }, */
      {
        dataField: 'distance',
        caption: 'Distance',
        allowHeaderFiltering: false,
        calculateDisplayValue: (event: Event) => event.distance.toFixed(0) + ' m',
        sortOrder: 'asc'
      },
      {
        dataField: 'location.adress',
        caption: 'Address',
        allowHeaderFiltering: false
      }
    ]
  }

  ngOnInit(): void {
    this.venueEvents = this.venuesService.getNearbyEvents(this.venue);
  }

}
