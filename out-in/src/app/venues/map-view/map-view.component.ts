import { Component, OnInit, Output, EventEmitter, Input, AfterViewInit } from '@angular/core';
import { Map, MapMouseEvent } from 'mapbox-gl';
import { Venue, Event } from '../venues.models';
import { VenuesService } from '../venues.service';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit, AfterViewInit {
  @Input() selectedVenue: Venue | null = null;
  @Output() selectionChange = new EventEmitter<Venue>();
  // init settings
  map!: Map;
  center: [number, number] = [4.833921, 52.3547498];
  zoom: [number] = [9];

  // venues settings
  venues: Venue[] = [];
  venuesLayout: any = {
    'icon-image': 'cafe',
    'icon-allow-overlap': false,
    'text-field': ['get', 'title'],
    'text-variable-anchor': ['top'],
    'text-radial-offset': 1,
    'text-justify': 'auto',
    'text-allow-overlap': true
  };
  venuesPaint: any = {
    'text-color': [
      'case',
      ['boolean', ['feature-state', 'selected'], false],
      "#f5429e",
      "#000000"
    ],
  };
  venuesFilter: any[] | undefined = undefined;
  venueFeatures: any = {
    type: 'FeatureCollection',
    features: []
  };
  selectedStateId: string | null = null

  // events settings
  eventsLayout: any = {
    'icon-image': 'event',
    'icon-allow-overlap': false,
    'text-field': ['get', 'title'],
    'text-variable-anchor': ['top'],
    'text-radial-offset': 1,
    'text-justify': 'auto',
    'text-allow-overlap': true
  };
  eventFeatures: any = {
    type: 'FeatureCollection',
    features: []
  };

  venueImageLoaded: boolean = false;
  eventImageLoaded: boolean = false;

  constructor(
    private venuesService: VenuesService) { 

    this.venues = this.venuesService.getVenues();
    this.venueFeatures.features = <any>this.venues.map(v => {
      return {
        type: 'Feature',
        id: v.trcid,
        properties: {
          title: v.title,
          venue: v
        },
        geometry: {
          type: 'Point',
          coordinates: [
            Number(v.location.longitude.replace(',', '.')), 
            Number(v.location.latitude.replace(',', '.'))
          ]
        }
      }
    })
    console.log(this.venueFeatures)
  }

  activateCursor(e: MapMouseEvent) {
    this.map.getCanvas().style.cursor = 'pointer';
  }

  disableCursor() {
    this.map.getCanvas().style.cursor = '';
  }

  onFilterByNameInput(e: any) {
    const text = e.target.value;
    if (text) {
      this.venuesFilter = ["in", text, ["get", "title"]]
    } else {
      this.venuesFilter = undefined
    }
  }

  venueClick(e: any) {
    if (e.features[0].properties.venue) {
      this.center = e.features[0].geometry.coordinates;
      this.zoom = [17];
      this.selectedVenue = <Venue>JSON.parse(e.features[0].properties.venue);
      this.selectionChange.emit(this.selectedVenue);
      if (this.selectedStateId !== null) {
        this.map.setFeatureState(
          { source: 'venuesSource', id: this.selectedStateId },
          { 'selected': false }
        )
      }
      this.selectedStateId = this.selectedVenue.trcid;
      this.map.setFeatureState(
        { source: 'venuesSource', id: this.selectedStateId },
        { 'selected': true }
      )
      
      this.createEventFeatures();
    } else {
      this.center = e.features[0].geometry.coordinates;
      this.zoom = [15];
    }        
  }

  ngOnInit(): void {
    if (this.selectedVenue) {
      this.center = [
        Number(this.selectedVenue.location.longitude.replace(',', '.')), 
        Number(this.selectedVenue.location.latitude.replace(',', '.'))
      ]
      this.zoom = [17]

      this.createEventFeatures();
    }
  }

  createEventFeatures() {
    if (this.selectedVenue) {
      this.eventFeatures = {
        type: 'FeatureCollection',
        features: []
      };
      const nearbyEvents = this.venuesService.getNearbyEvents(this.selectedVenue);
      this.eventFeatures.features = nearbyEvents.map(ev => {
        return {
          type: 'Feature',
          id: ev.trcid,
          properties: {
            title: ev.title,
            venue: ev
          },
          geometry: {
            type: 'Point',
            coordinates: [
              Number(ev.location.longitude.replace(',', '.')), 
              Number(ev.location.latitude.replace(',', '.'))
            ]
          }
        }
      });
    }
  }

  ngAfterViewInit() {

  }

}
