import { Component, HostListener, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'out-in';
  @ViewChild('page') page!: ElementRef<any>;  
  @ViewChild('inner') inner!: ElementRef<any>;

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this.refreshInnerHeight();
  }

  ngAfterViewInit(): void {
    this.refreshInnerHeight();

    const resizeObserver = new ResizeObserver(() => {
        setTimeout(() => this.refreshInnerHeight());
    });
  }

  refreshInnerHeight(): void {
    const innerHeight = this.inner.nativeElement.clientHeight;

    this.page.nativeElement.style.height = innerHeight + 'px';
    this.page.nativeElement.style.overflowY = 'unset';
  }
}

